/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class GameWinner extends Winner {
    
    private Player player;
    private int gamesWon;
    
    public GameWinner(){}
    
    public GameWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public String toString(){
        
        return "\n\n  ********** G A M E  W I N N E R ********** \n\n" + "\t" + player.getUserName() ;
    }
}
