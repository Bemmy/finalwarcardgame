/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public abstract class Winner {
    
    private Player player;
    
    public Winner(){}
    
    public Winner(Player player){
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
}
