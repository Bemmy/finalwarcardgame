/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class SetWinner extends Winner {
    
    private Player player;
    
    public SetWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public void displaySetWinner(){
        
        System.out.println("Set Winner is: " + player.getUserName());
    }
}
