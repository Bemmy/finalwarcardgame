/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

import java.util.List;
import java.util.ArrayList;

public class Hand {
    
    private Deck playerDeck;
    
    private Deck playerWinningsDeck;
    
    public Hand(){}
    
    public Hand(Deck playerDeck){
        
        this.playerDeck = playerDeck;
        playerWinningsDeck = playerDeck.createWinningsDeck();
   
    }
    
    public  void splitDeck(Deck gameDeck, Deck player1Deck, Deck player2Deck){
        
        List<Card> deckList = gameDeck.getCards();
        
        List<Card> player1 = new ArrayList<Card>();
        List<Card> player2 = new ArrayList<Card>();
        
        List<Card> player1WarHand = new ArrayList<Card>();
        List<Card> player2WarHand = new ArrayList<Card>();
        
        int size = gameDeck.getCards().size();
           
        for(int i = 0; i < (size/2); i++){
            player1.add(deckList.get(i));
        } 
        
        for(int i = (52 / 2); i < size; i++){
            player2.add(deckList.get(i));
        }
       
        player1Deck.setCards(player1);
        player2Deck.setCards(player2);
    
        player1Deck.setWarHand(player1WarHand);
        player2Deck.setWarHand(player2WarHand);
    }
    
    public Card playCard() {
     
         return playerDeck.getCards().get(0);
    }
    
    public void  callThreeCards() {
        
       List<Card> tempWarHand = new ArrayList<Card>();
        

        for (int i = 0; i < 3; i++){
            
            tempWarHand.add(playerDeck.getCards().get(i)); 
            
        } 
        
        playerDeck.setWarHand(tempWarHand);
        
        for(int i = 0; i < 3; i++){             playerDeck.getCards().remove(0); }

    }

    
    public Deck getPlayerDeck(){
        return playerDeck;
    }
    
    
    public Deck getPlayerWinningsDeck(){
        return playerWinningsDeck;
    }
    
    public String toString(){
        
        return "Player one deck: " + playerDeck.toString();
    }
}
