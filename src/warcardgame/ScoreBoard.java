/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class ScoreBoard {
    
    private Player player1;
    private Player player2;
    
    public ScoreBoard(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
    }
    
    public Player getPlayer1(){
        return player1;
    }
    
    public Player getPlayer2(){
        return player2;
    }
    
    public String toString(){
        return 
                "\n\n ~~~~~~~~~~~~~~~~~~~~~~  GAME SCORE BOARD  ~~~~~~~~~~~~~~~~~~~~~~" 
                + "\n\n::::::::::: Player 1 : " + player1.getUserName() + " ::::::::::: Rounds Won: " + 
                player1.getRoundWins() + " ::::::::::: War Rounds Won: " + player1.getWarRoundWins() +
                ":::::::::::" + "\n\n:::::::::::Player 2 : " + player2.getUserName() + " ::::::::::: Rounds Won: " + 
                player2.getRoundWins() + " ::::::::::: War Rounds Won: " + player2.getWarRoundWins() + ":::::::::::\n";
    }
}
