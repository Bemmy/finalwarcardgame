/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Deck {
    
    private List<Card> cards;
    private List<Card> cardsWon;
    private List<Card> warHand;


    public Deck(){
    
        cards = new ArrayList<Card>();
        
        for(CardSuit s : CardSuit.values()){
            for(CardValue v : CardValue.values()){
                Card card = new Card(s, v);
                cards.add(card);
                
            }
        }
       
    }
    
  public void shuffleDeck(Deck deck){

       // Deck shuffledGameDeck = new Deck();
        
        Collections.shuffle(cards);
        
        deck.setCards(cards);

       // System.out.println(shuffledGameDeck);
        
      //return shuffledGameDeck;
  }
    
    public Deck(List<Card> cardsWon){
        this.cardsWon = cardsWon;
    }
    
    public List<Card> getCards(){
        return cards;
    }
    
    public void setCards(List<Card> cards){
        this.cards = cards;
    }
    
    public Deck createWinningsDeck(){
        cardsWon = new ArrayList<Card>();
        
        return new Deck(cardsWon);
    }
    
    public void addToWinningsDeck(Card... cards){
        for(Card c : cards){
            cardsWon.add(c);
        }
    }
    
    public List<Card> getCardsWon(){
        return cardsWon;
    }
    
    
    public void removeCardFromDeck(){
        cards.remove(0);
    } 
    
    public List<Card> getWarHand(){
        return warHand;
    }
    
    public void setWarHand(List<Card> warHand){
        this.warHand = warHand;
    }
    
    public String toString(){
        
        return "Playing deck: " + cards + "Deck of winnings: " + cardsWon; 
    }
}
