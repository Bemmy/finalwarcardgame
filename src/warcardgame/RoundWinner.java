/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class RoundWinner extends Winner {
    
    private Player player;
    
    public RoundWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public String toString(){
        
        return "Round Winner is: " + player.getUserName();
    }
    
}
