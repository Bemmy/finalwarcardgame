/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

import java.util.ArrayList;
import java.util.List;

public class WarCardGame {

  
    public static void main(String[] args) {
                
                Player player1 = new Player("Hanz Razzles", 0,0);
                Player player2 = new Player("Danny Lowpass", 0,0);

                Deck gameDeck = new Deck();
                gameDeck.shuffleDeck(gameDeck);  /// FIX 
                
                Deck player1Deck = new Deck();
                Deck player2Deck = new Deck();
                Hand gameHand = new Hand();

                gameHand.splitDeck(gameDeck, player1Deck, player2Deck);
                                
                Hand player1Hand = new Hand(player1Deck);
                
                // System.out.println(player1Hand.getPlayerDeck().getCards().get(0));
                 
                Hand player2Hand = new Hand(player2Deck);
                
                CardComparison compareUnit = new CardComparison(player1, player1Hand, player2, player2Hand);
                
                compareUnit.compareSingleRound(player1Hand, player2Hand);
                
             
                
            }
        }
    
   
