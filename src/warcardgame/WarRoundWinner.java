/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;

public class WarRoundWinner extends Winner {
    
private Player player;
    
    public WarRoundWinner(){}
    
    public WarRoundWinner(Player player){
        super(player);
        this.player = player;
    }
    
    public Player getPlayer(){
        return player;
    }
    
    public void setPlayer(){
        this.player = player;
    }
    
    public String toString(){
        
        return "War Round Winner is: " + player.getUserName();
    }
    
}