/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warcardgame;
import java.util.ArrayList;
import java.util.List;

public class CardComparison {
    
    private Player player1;
    private Hand player1Hand;
    
    private Player player2;
    private Hand player2Hand;
    
    private List<Card> roundWinnings;
    
    
    public CardComparison(Player player1, Hand player1Hand, Player player2, Hand player2Hand){
        this.player1 = player1;
        this.player1Hand = player1Hand;
        
        this.player2 = player2;
        this.player2Hand = player2Hand;
    }
    
    public List<Card> getRoundWinnings(){
        return roundWinnings;
    }
    
    public Hand getPlayer1Hand(){
        return player1Hand;
    }
    
    public Hand getPlayer2Hand(){
        return player2Hand;
    }
    
    public void addCardsWon(Hand playerHand){
        
        if(playerHand.getPlayerWinningsDeck().getCardsWon().size() > 0){
            
        for(int i = 0; i < playerHand.getPlayerWinningsDeck().getCardsWon().size(); i++){
            
           playerHand.getPlayerDeck().getCards().add(playerHand.getPlayerWinningsDeck().getCardsWon().get(i));
        }
        
        playerHand.getPlayerWinningsDeck().getCardsWon().clear();
        
        }
    }
    
    public void chooseGameWinner(Player player){
        
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
       
        Winner gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player));
        
        System.out.println(gameWinner);
      
    }
    
    
    
    public void compareSingleRound(Hand player1Hand, Hand player2Hand){
        
        roundWinnings = new ArrayList<Card>();
        
        boolean empty = false;
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
        Winner gameWinner = new GameWinner();
        
        while(empty != true){
    
           addCardsWon(player1Hand);
               
        player1Hand.getPlayerDeck().shuffleDeck(player1Hand.getPlayerDeck());
        
        addCardsWon(player2Hand);
   
        player2Hand.getPlayerDeck().shuffleDeck(player2Hand.getPlayerDeck()); 
             
        Card player1Card = null;
                
        if(player1Hand.getPlayerDeck().getCards().size() > 0){    
            
        player1Card = player1Hand.playCard();  
        
        player1Hand.getPlayerDeck().getCards().remove(0);           

        }
        
        
        Card player2Card = null;
        
        if(player2Hand.getPlayerDeck().getCards().size() > 0){
            
        player2Card = player2Hand.playCard();
        
        player2Hand.getPlayerDeck().getCards().remove(0);
        } 

        if(player1Card != null && player2Card != null){
        System.out.println(player1Card + " vs. " + player2Card);
        
        roundWinnings.add(player1Card);
        roundWinnings.add(player2Card);
        
        int player1CardValue = player1Card.getValue().getIntValue();
        int player2CardValue = player2Card.getValue().getIntValue();
        
        if(player1CardValue == player2CardValue){
            
            addCardsWon(player1Hand);
               
        player1Hand.getPlayerDeck().shuffleDeck(player1Hand.getPlayerDeck()); 
        
       addCardsWon(player2Hand);
   
        player2Hand.getPlayerDeck().shuffleDeck(player2Hand.getPlayerDeck()); 
        
            
            System.out.println("-------------- R O U N D        O F       W A R --------------");
            
           if(player1Hand.getPlayerDeck().getCards().size() < 3) {
               
              chooseGameWinner(player2);
               
           } else if (player2Hand.getPlayerDeck().getCards().size() < 3){
               
               chooseGameWinner(player1);
               
           } else {
            
           compareMultipleWarRound();
           
        }
           
        } else if(player1CardValue > player2CardValue){
        
         for (int i = 0; i < roundWinnings.size(); i++) {
             player1Hand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(i));
         }
        
        Winner roundWinner = (RoundWinner)(winnerFactory.getWinner(WinnerType.ROUNDWINNER, player1));
        
        System.out.println(roundWinner + "\n");
        
        roundWinnings.clear();
    
        } else {
             
         for (int i = 0; i < roundWinnings.size(); i++) {
             player2Hand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(i));
         }    
             
            Winner roundWinner = (RoundWinner)(winnerFactory.getWinner(WinnerType.ROUNDWINNER, player2));
            System.out.println(roundWinner + "\n");
            
            roundWinnings.clear();
      
       }  
     
        
        } else if (player1Hand.getPlayerDeck().getCards().size() == 0) { 
            gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player2));
            System.out.println(gameWinner);
            empty = true;
        } else {
            gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player1));
            System.out.println(gameWinner);
            empty = true;
        }
        
        }
    }
        
    public void collectPlayer1WarRoundCards(Hand player1Hand) { 
      
        player1Hand.getPlayerDeck().getWarHand().clear();
        
        player1Hand.callThreeCards();
                 
        for(int i = 0; i < player1Hand.getPlayerDeck().getWarHand().size(); i++){
           
            roundWinnings.add(player1Hand.getPlayerDeck().getWarHand().get(i));
        }
         
        }
    
        public void collectPlayer2WarRoundCards(Hand player2Hand) {
        
        player2Hand.getPlayerDeck().getWarHand().clear();
                
        player2Hand.callThreeCards();
                 
        for(int i = 0; i < player2Hand.getPlayerDeck().getWarHand().size(); i++){
           
            roundWinnings.add(player2Hand.getPlayerDeck().getWarHand().get(i));
        }
        
    }
       
        
    public void compareMultipleWarRound() {
       
        WinnerFactory winnerFactory = WinnerFactory.getInstance();
        Winner warRoundWinner = new WarRoundWinner();
        Winner gameWinner = new GameWinner();

        int player1Wins = 0;
        int player2Wins = 0;
        boolean tie = false;
       
        while(tie != true){
            
        if(player1Hand.getPlayerWinningsDeck().getCardsWon().size() > 0){
            
            for(int i = 0; i < player1Hand.getPlayerWinningsDeck().getCardsWon().size(); i++){
                player1Hand.getPlayerDeck().getCards().add(player1Hand.getPlayerWinningsDeck().getCardsWon().get(i));
            } 
                             player1Hand.getPlayerWinningsDeck().getCardsWon().clear();

        }
        
             if(player2Hand.getPlayerWinningsDeck().getCardsWon().size() > 0){
            for(int i = 0; i < player2Hand.getPlayerWinningsDeck().getCardsWon().size(); i++){
                player2Hand.getPlayerDeck().getCards().add(player2Hand.getPlayerWinningsDeck().getCardsWon().get(i));
            }
            
               player2Hand.getPlayerWinningsDeck().getCardsWon().clear();
               
        
        }
            
            collectPlayer1WarRoundCards(player1Hand);
            
            collectPlayer2WarRoundCards(player2Hand);

      
             for(int i = 0; i < 3; i++){   
                 
                    System.out.println("WAR: " + player1Hand.getPlayerDeck().getWarHand().get(i) + " vs. " + player2Hand.getPlayerDeck().getWarHand().get(i));
     
        if(player1Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue() > player2Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue()){
            
           player1Wins++;
            
            tie = true;
            
        } else if(player1Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue() == player2Hand.getPlayerDeck().getWarHand().get(i).getValue().getIntValue()) {
            
            if(player1Hand.getPlayerDeck().getCards().size() < 3){ 
                
                gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player2));
                System.out.println(gameWinner);
                
            } else if(player2Hand.getPlayerDeck().getCards().size() <3) { 
                gameWinner = (GameWinner)(winnerFactory.getWinner(WinnerType.GAMEWINNER, player1));
                System.out.println(gameWinner);
            } else {
            tie = false;
            continue;
            }
            
        } else {
            
            player2Wins++;
            
            tie = true;
            
        }
   
        if(player1Wins > player2Wins){
            
            warRoundWinner = (WarRoundWinner)(winnerFactory.getWinner(WinnerType.WARROUNDWINNER, player1));
            
            for(int k = 0; k < roundWinnings.size(); k++){
                player1Hand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(k));   
            }
              roundWinnings.clear();
            
            System.out.println(warRoundWinner);
            
            player1.setWarRoundWins(player1.getWarRoundWins()+ 1);
            
        } else {
            
            warRoundWinner = (WarRoundWinner)(winnerFactory.getWinner(WinnerType.WARROUNDWINNER, player2));
            for(int j = 0; j < roundWinnings.size(); j++){
                player2Hand.getPlayerWinningsDeck().getCardsWon().add(roundWinnings.get(j));
            }
            
            roundWinnings.clear();

                         System.out.println(warRoundWinner);
                         
           player2.setWarRoundWins(player2.getWarRoundWins()+ 1);


        }
             
        } 
           
        } 
        }
}
        
    


